(ns string-calc.core
  (:require [clojure.string :as str]))

(defn parse-int [s]
  (Integer/parseInt (re-find #"\d+" s)))

(defn split-to-ints [s]
  (map parse-int (str/split s #",")))

(defn add-string [s]
  "Takes a string s delimited with commas, and adds them up."
  (if (= (count s) 0)
    s
    (str (reduce + (split-to-ints s)))))
