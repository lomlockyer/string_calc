(ns string-calc.core-test
  (:require [clojure.test :refer :all]
            [string-calc.core :refer :all]))

(deftest test-empty
  (testing "empty string")
  (is (= (add-string "") "")))

(deftest one-number
  (testing "one number should return itself")
  (is (= (add-string "4") "4")))

(deftest two-numbers
  (testing "two numbers should be added")
  (is (= (add-string "4,3") "7")))

(deftest many-numbers
  (testing "many numbers should be added together")
  (is (= (add-string "4,3,7,6") "20")))

(deftest many-numbers-with-spaces
  (testing "many numbers with spaces should be added together")
  (is (= (add-string "4  ,    3, 7  , 6") "20")))
